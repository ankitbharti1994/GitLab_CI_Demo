//
//  UsingCustomTextFontTests.m
//  UsingCustomTextFontTests
//
//  Created by Adi Boddapati on 14/04/17.
//  Copyright © 2017 KaHa. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface UsingCustomTextFontTests : XCTestCase

@end

@implementation UsingCustomTextFontTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)testForAddition {
    NSInteger first=4;
    NSInteger second=5;
    NSInteger addition=first+second;
    
    XCTAssertEqual(addition, 9,@"Not equal");
}

@end
