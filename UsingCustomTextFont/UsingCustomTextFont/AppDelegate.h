//
//  AppDelegate.h
//  UsingCustomTextFont
//
//  Created by Ankit Bharti on 14/04/17.
//  Copyright © 2017 KaHa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

