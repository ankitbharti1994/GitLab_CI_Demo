//
//  ViewController.m
//  UsingCustomTextFont
//
//  Created by Ankit Bharti on 14/04/17.
//  Copyright © 2017 KaHa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    for (NSString *family in [UIFont familyNames]) {
        NSLog(@"family Name--> %@",family);
    }
    [self getFontName];
    [self changeTextFont:@"Roboto-Black" fontSize:14.0 text:@"Roboto-Black" onLabel:self.label1];
    [self changeTextFont:@"Roboto-BlackItalic" fontSize:14.0 text:@"Roboto-BlackItalic" onLabel:self.label2];
    [self changeTextFont:@"Roboto-Bold" fontSize:14.0 text:@"Roboto-Bold" onLabel:self.label3];
    [self changeTextFont:@"Roboto-BoldItalic" fontSize:14.0 text:@"Roboto-BoldItalic" onLabel:self.label4];
    [self changeTextFont:@"Roboto-Italic" fontSize:14.0 text:@"Roboto-Italic" onLabel:self.label5];
    [self changeTextFont:@"Roboto-Light" fontSize:14.0 text:@"Roboto-Light" onLabel:self.label6];
    [self changeTextFont:@"Roboto-LightItalic" fontSize:14.0 text:@"Roboto-LightItalic" onLabel:self.label7];
    [self changeTextFont:@"Roboto-Medium" fontSize:14.0 text:@"Roboto-Medium" onLabel:self.label8];
    [self changeTextFont:@"Roboto-MediumItalic" fontSize:14.0 text:@"Roboto-MediumItalic" onLabel:self.label9];
    [self changeTextFont:@"Roboto-Regular" fontSize:14.0 text:@"Roboto-Regular" onLabel:self.label10];
    [self changeTextFont:@"Roboto-Thin" fontSize:14.0 text:@"Roboto-Thin" onLabel:self.label11];
    [self changeTextFont:@"Roboto-ThinItalic" fontSize:14.0 text:@"Roboto-ThinItalic" onLabel:self.label12];
}

-(void)getFontName {
    for (NSString *family in [UIFont familyNames]) {
        for (NSString *font in [UIFont fontNamesForFamilyName:family]) {
            NSLog(@"font --> %@",font);
        }
    }
}

-(void)changeTextFont:(NSString *)fontName fontSize:(CGFloat)size text:(NSString *)text onLabel:(UILabel *)label{
    label.text=text;
    label.font=[UIFont fontWithName:fontName size:size];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
